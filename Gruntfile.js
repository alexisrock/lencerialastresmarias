module.exports = function (grunt){
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt,{
        useminPrepare: 'grunt-usemin'
    });
grunt.initConfig({
    sass: {
        dist: {
            files: [{
                expand: true,
                cwd: 'css',
                src: ['*.scss'],
                dest: 'css',
                ext: '.css'
            }]
        }
    },
    watch: {
        files: ['css/*.scss'],
        tasks: ['css']
    },
    browserSync: {
        dev: {
            bsFiles: {
                src: [
                    'css/*.css',
                    '*.html',
                    'js/*.js'
                ]
            },
            opotions: {
                watchTask: true,
                server: {
                    baseDir: './'
                }
            }
        }
    },
    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: './',
                src: 'Imagenes/*.{png, gif. jpg, jpeg}',
                dest: 'dist/',
                ext: '.css'
            }]
        }
    },
    copy: {
        html: {
            files:[{
                expand: true,
                dot: true,
                cwd: './',
                src: ['*.html'],
                dest: 'dist',
          
            }]
        },
        font:{
            files: {
                expand: true,
                dot: true,
                cwd: 'node_modules/open-iconic/font',
                src: ['font/*.*'],
                dest: 'dist',
            }
        }
    },
    clean: {
        build: {
            src: {'dist/'}
        }
    },
    cssmin: {
        dist: {}
    },
    uglify: {
        dist: {}
    },
    filerev: {
        options: {
            enconding: 'utf8',
            algoritm: 'md5',
            length: 20
        },
        release: {
            files: {
                src: [
                    'dist/js/*.js',
                    'dist/css/*.css'
                ] 
            }
        }
    },
    concat: {
        options: {
            separator: ';'
        },
        dist: {}
    },
    useminPrepare: {
        foo: {
            dest: 'dist',
            src: ['index.html', 'acercade.html', 'contactenos.html','productos.html']
        },
        options: {
            flow: {
                steps: {
                    css: ['cssmin'],
                    js: ['uglify']
                },
                post: {
                    css: [{
                        name: 'cssmin',
                        createConfig: function(context, block){
                            var generated = context.options.generated;
                            generated.options = {
                                keepSpecialComments: 0,
                                rebase: false
                            }
                        }
                    }]
                } 
            }
        }
    },
    usemin: {
        htlm: ['dist/index.html', 'dist/acercade.html', 'dist/contactenos.html','dist/productos.html'],
        options: {
            assetsDir: ['dist', 'dist/css', 'dist/js']
        }
    }

});

grunt.registerTask('css',['sass']);
grunt.registerTask('default',['browserSync', 'watch']);
grunt.registerTask('img:compress',['imagemin']);
grunt.registerTask('build' ,['clean', 'copy', 'imagemin', 'useminPrepare','concat','cssmin', 'uglify','filerev','usemin']);
};